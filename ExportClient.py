import sys
import requests
import json
import os
from base64 import b64encode
import ConfigParser
from datetime import datetime, timedelta
import dateutil.parser


r3_url = "https://usea1.r3.securitycloud.symantec.com/r3_epmp_i"
oauth_url = "/oauth2/tokens"
export_api = "/sccs/v1/events/export"
CONFIG_INI = os.path.join('/Applications/Splunk/', 'bin', 'scripts', 'SEPCloudConfig.ini')
START_DATE = 'start_date'
END_DATE = 'end_date'
CONFIG_EVENTS_SECTION = 'Events'
BATCH_SIZE = 'batch_size'
TYPE = 'event_type_filter'
CONFIG_AUTHENTICATION_SECTION = 'Authentication'
CLIENT_ID = 'client_id'
CLIENT_SECRET = 'client_secret'


# Function to build base headers
def build_base_headers():
    headers = {"Accept": "application/json"}
    return headers


# Function to get OAuth token
def get_oauth_token(client_id, client_secret):

    headers = build_base_headers()
    headers.update({"Content-Type": "application/x-www-form-urlencoded"})

    token = b64encode(client_id + ":" + client_secret).decode("ascii")
    headers.update({"Authorization": "Basic " + token})
    params = {'grant_type': 'client_credentials'}

    response = requests.post("%s%s" % (r3_url, oauth_url),
                             headers=headers,
                             data=params)
    if response.status_code == 200:
        data = response.json()
        return data['access_token']

    return None


# Function to update config
def update_config_ini(start_date, end_date, batch_size, event_type, client_id, client_secret):
    config = ConfigParser.RawConfigParser()
    config.add_section(CONFIG_EVENTS_SECTION)
    config.add_section(CONFIG_AUTHENTICATION_SECTION)
    config.set(CONFIG_EVENTS_SECTION, START_DATE, start_date)
    config.set(CONFIG_EVENTS_SECTION, END_DATE, end_date)
    config.set(CONFIG_EVENTS_SECTION, TYPE, event_type)
    config.set(CONFIG_EVENTS_SECTION, BATCH_SIZE, batch_size)
    config.set(CONFIG_AUTHENTICATION_SECTION, CLIENT_ID, client_id)
    config.set(CONFIG_AUTHENTICATION_SECTION, CLIENT_SECRET, client_secret)

    with open(CONFIG_INI, 'wb') as configfile:
        config.write(configfile)


# Function to export events
def export_events(token, event_type, batch_size, start_date, end_date, client_id, client_secret):

    data = None

    headers = build_base_headers()
    headers.update({"Content-Type": "application/json"})
    headers.update({"Authorization": token})

    keys = ["type", "batchSize", "startDate", "endDate"]
    values = [event_type, int(batch_size), start_date, end_date]

    params = {}
    for index in range(len(keys)):
        params[keys[index]] = values[index]

    params = json.dumps(params)
    response = requests.post("%s%s" % (r3_url, export_api),
                             headers=headers,
                             data=params)

    if response.status_code == requests.codes.ok:
        data = response.json()
    # TODO: To Test
    elif response.status_code == 401:
        token = get_oauth_token(client_id, client_secret)
        export_events(token, event_type, batch_size, start_date, end_date, client_id, client_secret)

    return data


# Function to read file
def read_configuration_file():
    config = ConfigParser.SafeConfigParser()
    config.read(CONFIG_INI)
    return config


# Function to verify valid event type
def verify_event_type(config, event_type):
    if event_type is None or event_type == "":
        event_type = "all"
    else:
        event_type = config.get(CONFIG_EVENTS_SECTION, TYPE)
    return event_type


# Function to verify batch size
def verify_batch_size(batch_size, config):
    if batch_size is None or batch_size == "":
        batch_size = 100
    else:
        batch_size = config.get(CONFIG_EVENTS_SECTION, BATCH_SIZE)
    return batch_size


# Function to read configurations from file
def read_configurations(config):
    start_date = config.get(CONFIG_EVENTS_SECTION, START_DATE)
    end_date = config.get(CONFIG_EVENTS_SECTION, END_DATE)
    batch_size = config.get(CONFIG_EVENTS_SECTION, BATCH_SIZE)
    event_type = config.get(CONFIG_EVENTS_SECTION, TYPE)
    client_id = config.get(CONFIG_AUTHENTICATION_SECTION, CLIENT_ID)
    client_secret = config.get(CONFIG_AUTHENTICATION_SECTION, CLIENT_SECRET)
    return batch_size, client_id, client_secret, end_date, event_type, start_date


# Function to update configurations in file
def update_configuration(batch_size, client_id, client_secret, end_date, event_type):
    start_date = end_date
    end_date = datetime.today().utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"
    update_config_ini(start_date, end_date, batch_size, event_type, client_id, client_secret)


# Function to parse date
def parse_date(date_to_parse, default_date_time):
    try:
        if (date_to_parse is None) or (date_to_parse == ""):
            date_to_parse = default_date_time.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"
        else:
            date_to_parse = dateutil.parser.parse(date_to_parse).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4] + "Z"

        return date_to_parse

    except Exception:
        return None


# Function to verify date
def verify_date(end_date, start_date):

    try:
        curr_date = datetime.today().utcnow()
        parsed_start_date = parse_date(start_date, (curr_date - timedelta(days=1)))
        parsed_end_date = parse_date(end_date, curr_date)

        if parsed_start_date is None or parsed_end_date is None or parsed_end_date <= parsed_start_date:
            raise ValueError('End Date should be higher than Start Date.')

    except Exception:
        sys.exit(1)

    return parsed_end_date, parsed_start_date


def main():
    config = read_configuration_file()
    if config is None:
        sys.exit(1)

    batch_size, client_id, client_secret, read_end_date, event_type, read_start_date = read_configurations(config)

    if client_id is None or client_secret == "":
        sys.exit(1)
    else:
        token = get_oauth_token(client_id, client_secret)
        if token is None:
            sys.exit(1)

    batch_size = verify_batch_size(batch_size, config)
    event_type = verify_event_type(config, event_type)
    end_date, start_date = verify_date(read_end_date, read_start_date)

    total_events = 0
    while True:
        data = export_events(token, event_type, batch_size, start_date, end_date, client_id, client_secret)

        total_events = total_events + len(data)
        if data is None:
            break
        elif len(data) == 0:
            break
        else:
            for event in data:
                print json.dumps(event)
                print('\n')
                sys.stdout.flush()

    update_configuration(batch_size, client_id, client_secret, end_date, event_type)


main()
